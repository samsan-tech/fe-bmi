$.ajax({
    url: `http://104.154.115.148:8000/api/users/${getId()}`,
    type: "GET",
    processData: false,
    contentType: false,
    success: function (result) {
        let data = result.data;

        document.getElementById("form").insertAdjacentHTML(
            "afterend",
            `
            <form onsubmit="event.preventDefault(); handleUpdate(this);">
            <div class="form-group">
              <label for="name">Name</label>
              <input
                name="name"
                class="form-control"
                type="text"
                placeholder="isi nama"
                value=${data.name}
              />
            </div>
            <div class="form-group">
              <label for="name">Weight</label>
              <input
                name="weight"
                class="form-control"
                type="number"
                placeholder="isi berat badan"
                value=${data.weight}
              />
            </div>
            <div class="form-group">
              <label for="name">Height</label>
              <input
                name="height"
                class="form-control"
                type="number"
                placeholder="isi tinggi badan"
                value=${data.height}
              />
            </div>
            <div class="form-group">
              <label for="age">Age</label>
              <input name="age" class="form-control" type="text" value=${data.age} placeholder="isi umur" />
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>`
        );
    },
});

function handleUpdate(e) {
    const { name, age, weight, height } = e.elements;
    let datas = {
        name: name.value,
        age: age.value,
        weight: weight.value,
        height: height.value,
    };

    $.ajax({
        url: "http://104.154.115.148:8080/update-user",
        type: "POST",
        dataType: "json",
        data: datas,
        success: function (result) {
            if (result.message) {
                alert(`${name.value} berhasil di-update`);
                window.location.reload();
            } else {
                alert("gagal");
            }
        },
    });
}

function getId() {
    let url = window.location.pathname.split("/");
    let id = url[2];
    return id;
}
