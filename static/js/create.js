function handleCreate(e) {
  const { name, age, weight, height, date } = e.elements;
  let datas = {
    name: name.value,
    age: age.value,
    weight: weight.value,
    height: height.value,
    update_bmi: date.value,
  };

  $.ajax({
    url: "http://104.154.115.148:8080/create-user",
    type: "POST",
    dataType: "json",
    data: datas,
    success: function (result) {
      if (result.message) {
        alert(`${name.value} berhasil dimasukkan`);
        window.location.reload();
      } else {
        alert("gagal");
      }
    },
  });
}
