$.ajax({
    url: "http://104.154.115.148:8000/api/users",
    type: "GET",
    processData: false,
    contentType: false,
    success: function (result) {
        let datas = result.data;

        datas.forEach((data) => {
            if (data.need_update == true) {
                document.getElementById("user").insertAdjacentHTML(
                    "afterend",
                    ` <tr>
            <td>${data.name}</td>
            <td>${data.weight}</td>
            <td>${data.height}</td>
            <td>${data.age}</td>
            <td>${data.bmi}</td>
            <td>${data.need_update}</td>
            <td>
              <button type="button" id=${data._id} class="btn btn-primary" onclick="handleUpdatePage('${data._id}')">Update</button>
              <button type="button" id=${data._id} class="btn btn-danger" onclick="handleDelete('${data._id}', '${data.name}')" >Delete</button>
            </td>
          </tr>`
                );
            }
            else {
                document.getElementById("user").insertAdjacentHTML(
                    "afterend",
                    ` <tr>
            <td>${data.name}</td>
            <td>${data.weight}</td>
            <td>${data.height}</td>
            <td>${data.age}</td>
            <td>${data.bmi}</td>
            <td>${data.need_update}</td>
            <td>
              <button type="button" id=${data._id} class="btn btn-danger" onclick="handleDelete('${data._id}', '${data.name}')" >Delete</button>
            </td>
          </tr>`
                );
            }
        });
    },
});

function handleDelete(id, name) {
    $.ajax({
        url: "http://104.154.115.148:8000/api/users",
        type: "DELETE",
        data: {
            id: id,
            name: name,
        },
        dataType: "json",
        success: function (result) {
            console.log(result.message);
            if (result.message) {
                alert("success");
                window.location.reload();
            } else {
                alert(`failed to delete user dengan id ${id}`);
            }
        },
    });
}

function handleUpdatePage(id) {
    // alert("success");
    window.location.pathname = `/update-user/${id}`;
}
