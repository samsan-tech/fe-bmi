#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Flask, render_template

DEVELOPMENT_ENV = True

app = Flask(__name__)

app_data = {
    "name":         "BMI Calculator",
    "description":  "A Simple BMI Calculator App",
    "author":       "Samsan",
    "html_title":   "BMI Calculator",
    "project_name": "fe-bmi",
    "keywords":     "BMI"
}


@app.route('/')
def index():
    return render_template('index.html', app_data=app_data)


@app.route('/users')
def contact():
    return render_template('users.html', app_data=app_data)


@app.route('/add-user')
def addUser():
    return render_template('addForm.html', app_data=app_data)


@app.route('/update-user/<id>')
def updateUser(id):
    return render_template('updateForm.html', id=id, app_data=app_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
