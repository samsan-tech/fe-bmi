FROM python:3.7.7
ADD . /code
WORKDIR /code
EXPOSE 5000
RUN pip3 install -r requirements.txt
CMD ["python3", "-m", "flask", "run", "--host=0.0.0.0"]