# BMI Calculator

Simple App for counting your BMI

## Running Locally

Make sure you have Python installed

```sh
$ cd fe-bmi
$ pip install -r requirements.txt
$ python app.py
```

Your app should now be running on [localhost:5000](http://localhost:5000/).
